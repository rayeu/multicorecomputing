#ifndef PRIMEGENERATOR
#define PRIMEGENERATOR

/*
Prime number generation using C++ threads

Ashok Adhikari
*/

#include <iostream>
#include <thread>
#include <atomic>
#include <mutex>

using namespace std;

class PrimeGenerator {
private:
    int number;
    int nThreads;
    thread *workers = NULL;
    int counter;
    int primeCount;

    mutex primeCountLock;
    mutex counterLock;

public:
    PrimeGenerator (int, int);
    ~PrimeGenerator ();

    int computeSquareRoot (int);
    bool isPrime (int);
    
    void run ();
    int find ();

    int getNextNumber ();
    void incrementCount ();
};

#endif