/*
main.cpp for PrimeGenerator
*/

#include <string>
#include "primes.h"
#include <atomic>
#include <chrono>

using namespace std;

int main(int argc, char const *argv[])
{

    if (argc != 5) {
        printf("Usage: ./main -t <thread_count> -l <number>\n");
        exit (1);
    }

    if (string(argv[1]).compare(string("-t")) || string(argv[3]).compare(string("-l"))) {
        printf("Usage: ./main -t <thread_count> -l <number>\n");
        exit (1);
    }

    int n = atoi (argv[4]);
    int t = atoi (argv[2]);

    PrimeGenerator pg (n, t);

    auto t1 = chrono::high_resolution_clock::now();

    int numPrimes = pg.find ();

    auto t2 = chrono::high_resolution_clock::now();

    chrono::duration<double> elapsed_seconds = t2 - t1;

    cout << "Time taken = " << elapsed_seconds.count() << endl;

    cout << "Number of Primes upto " << n << " is " << numPrimes << endl;

    return 0;
}