
/*
primes.cpp implementation file

Ashok Adhikari
*/

#include "primes.h"

PrimeGenerator::PrimeGenerator (int number, int nThreads) {
    this->number = number;

    counter = 1;
    primeCount = 0;

    this->nThreads = nThreads;
    workers = new thread[nThreads];
}

PrimeGenerator::~PrimeGenerator () {
    delete [] workers;
}

int PrimeGenerator::computeSquareRoot (int n) {
    // compute square root of a number n using Newton method
    int prevEstimate, curEstimate;

    curEstimate = n;

    do {
        prevEstimate = curEstimate;
        curEstimate = (n / curEstimate + curEstimate) / 2;
    }
    while (curEstimate - prevEstimate > 1);

    return curEstimate;
}

bool PrimeGenerator::isPrime (int n) {
    int squareRoot = computeSquareRoot (n);

    // numbers less than 2 are not prime
    if (n < 2) return false;

    // number 2 is a prime, handled a special case
    if (n == 2) return true;

    // all even numbers are not prime
    if (n % 2 == 0) return false;

    // we then check if the number is divisible by remaining odd numbers
    // from 3 to sqrt(n)
    for (int i = 3; i <= squareRoot; i += 2)
        if (n % i == 0) return false;

    return true;
}

int PrimeGenerator::getNextNumber () {
    lock_guard <mutex> guard (counterLock);
    counter++;
    if (counter > number)
        return -1;
    return counter;
}

void PrimeGenerator::incrementCount () {
    lock_guard <mutex> guard (primeCountLock);
    primeCount++;
}

void PrimeGenerator::run () {
    while (true) {
        int numToCheck = getNextNumber ();
        // this means, all the numbers have been checked
        if (numToCheck == -1) break;
        // if any number remains, check for it
        if (isPrime (numToCheck))
            incrementCount ();
    }
}

int PrimeGenerator::find () {
    for (int i = 0; i < nThreads; i++) {
        workers[i] = thread ([ = ] {run();});
    }

    for (int i = 0; i < nThreads; i++) {
        workers[i].join ();
    }
    return primeCount;
}