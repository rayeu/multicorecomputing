//
//  lockinterface.hpp
//  Assignment2
//
//  Created by Ashok Adhikari on 4/17/16.
//  Copyright © 2016 unlv. All rights reserved.
//

#ifndef Locks_hpp
#define Locks_hpp

#include <stdio.h>

class ILock {
public:
    virtual void lock () = 0;
    virtual void unlock () = 0;
};

#endif /* Locks_hpp */
