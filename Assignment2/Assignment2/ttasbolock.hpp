//
//  ttasbolock.h
//  Assignment2
//
//  Created by Ashok Adhikari on 4/18/16.
//  Copyright © 2016 unlv. All rights reserved.
//

#ifndef ttasbolock_h
#define ttasbolock_h

#include "lockinterface.hpp"

#include <atomic>
#include <thread>

using namespace std;

class TTASBOLock: public ILock {
private:
    atomic_bool sharedAtomicBool;
    
public:
    TTASBOLock () {
        sharedAtomicBool.store(false);
    }
    
    void lock () {
        bool expected = false;
        while (true) {
            while (sharedAtomicBool.load()) {}
            
            if(sharedAtomicBool.compare_exchange_strong (expected, true, memory_order_acquire)) {
                return;
            } else {
                expected = false;
                this_thread::sleep_for(chrono::milliseconds(1));
            }
        }
    }
    
    void unlock () {
        sharedAtomicBool.store(false);
    }
};


#endif /* ttasbolock_h */
