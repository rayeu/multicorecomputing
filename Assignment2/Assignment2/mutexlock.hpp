//
//  mutexlock.hpp
//  Assignment2
//
//  Created by Ashok Adhikari on 4/17/16.
//  Copyright © 2016 unlv. All rights reserved.
//

#ifndef MUTEXLOCK_HPP
#define MUTEXLOCK_HPP

#include <iostream>
#include <mutex>

#include "lockinterface.hpp"

using namespace std;


class MutexLock: public ILock {
public:
    mutex m1;
    
    void lock () {
        m1.lock();
    }
    
    void unlock () {
        m1.unlock();
    }
};

#endif
