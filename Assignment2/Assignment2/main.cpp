//
//  main.cpp
//  Assignment2
//
//  Created by Ashok Adhikari on 4/17/16.
//  Copyright © 2016 unlv. All rights reserved.
//


#include "mutexlock.hpp"
#include "taslock.hpp"
#include "ttasbolock.hpp"
#include "benchmarker.hpp"

#include <thread>

enum LockType {lt_spin, lt_ttasbo, lt_mutex};


class Driver {
private:
    unsigned long long counter = 0;
    const unsigned long long targetCount = 1000000;
    ILock *lock;
    thread *runners;
    Benchmarker benchmarker;
    
public:
    Driver () {
        cout << "Initializing driver..DONE" << endl;
    }
    
    void run (const int threadCount, const enum LockType lockType) {
        benchmarkLock(threadCount, lockType);
        
        printCount();
        benchmarker.printResults();
    }
    
    void benchmarkLock (const int threadCount, const int lockType) {
        switch (lockType) {
            case lt_spin:
                lock = new TASLock ();
                break;
            case lt_mutex:
                lock = new MutexLock ();
                break;
            case lt_ttasbo:
                lock = new TTASBOLock ();
                break;
            default:
                break;
        }
        
        runners = new thread[threadCount];
        
        benchmarker.startBenhmarker();
        
        for (int i = 0; i < threadCount; i++) {
            runners[i] = thread ([ = ] {
                work(i, false);
            });
        }
        
        for (int i = 0; i < threadCount; i++) {
            runners[i].join ();
        }
        
        benchmarker.endBenchmarker();
    }
    
    void work (int threadid, bool debug=false) {
        while (counter < targetCount) {
            lock->lock();
            if (debug)
                cout << "Thread ID = " << threadid << " acquired the lock" << endl;
            if (counter < targetCount)
                counter++;
            lock->unlock();
        }
    }
    
    void printCount () {
        cout << "Counter Value = " << counter << endl;
    }
};


int main(int argc, const char * argv[]) {
    
    if (argc != 5) {
        printf("Usage: ./assignment2 -t <thread_count> -l <lock_type>\nlock_type = spin|ttasbo|mutex\n");
        exit (1);
    }
    
    if (string(argv[1]).compare(string("-t")) || string(argv[3]).compare(string("-l"))) {
        printf("Usage: ./assignment2 -t <thread_count> -l <lock_type>\nlock_type = spin|ttasbo|mutex\n");
        exit (1);
    }
    
    
    int threadCount = atoi (argv[2]);
    
    LockType lockType;

    string lName = string (argv[4]);
    if (!lName.compare("spin")) {
        lockType = lt_spin;
    } else if (!lName.compare("ttasbo")) {
        lockType = lt_ttasbo;
    } else if (!lName.compare("mutex")) {
        lockType = lt_mutex;
    } else {
        printf("Usage: ./assignment2 -t <thread_count> -l <lock_type>\nlock_type = spin|ttasbo|mutex\n");
        exit(1);
    }
    
    cout << "Thread count is " << threadCount << endl;
    cout << "Lock type is " << lName << endl;
    
    Driver driver;
    
    driver.run(threadCount, lockType);
    
    return 0;
}