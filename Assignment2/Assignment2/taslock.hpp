//
//  taslock.h
//  Assignment2
//
//  Created by Ashok Adhikari on 4/18/16.
//  Copyright © 2016 unlv. All rights reserved.
//

#ifndef taslock_h
#define taslock_h

#include "lockinterface.hpp"

#include <atomic>

using namespace std;

class TASLock: public ILock {
private:
    atomic_bool sharedAtomicBool;
    
public:
    TASLock () {
        sharedAtomicBool.store(false);
    }
    
    void lock () {
        bool expected = false;
        while (!sharedAtomicBool.compare_exchange_strong (expected, true, memory_order_acquire)) {
            expected = false;
        }
    }
    
    void unlock () {
        sharedAtomicBool.store(false);
    }
};

#endif /* taslock_h */
