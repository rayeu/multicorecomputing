//
//  benchmarker.h
//  Assignment2
//
//  Created by Ashok Adhikari on 4/17/16.
//  Copyright © 2016 unlv. All rights reserved.
//

#ifndef benchmarker_h
#define benchmarker_h

#include <iostream>
#include <atomic>
#include <chrono>

using namespace std;


class Benchmarker {
private:
    chrono::high_resolution_clock::time_point starttime;
    chrono::high_resolution_clock::time_point endtime;

public:
    void startBenhmarker () {
        cout << "Starting Benchmarker..DONE" << endl;
        starttime = chrono::high_resolution_clock::now();
    }
    
    void endBenchmarker () {
        endtime = chrono::high_resolution_clock::now();
        cout << "Ending Benchmarker..DONE" << endl;
    }
    
    void printResults () {
        cout << "Program took: " << chrono::duration_cast<chrono::milliseconds> (endtime - starttime).count() << " milliseconds" << endl;
    }
    
};


#endif /* benchmarker_h */
