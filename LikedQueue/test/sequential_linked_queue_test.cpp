#include <gtest/gtest.h>

#include "../src/sequential_linked_queue.h"


class SequentialLinkedQueueTest: public ::testing::Test {
public:
    SequentialLinkedQueueTest () {
        cout << "constructor of the fixture called " << endl;
    }

    void SetUp () {
        cout << "test setup setting up " << endl;
        SequentialLinkedQueue <int> intQueue;
    }

    void TearDown () {
        cout << "test fixture tearing down" << endl;
    }

    ~SequentialLinkedQueueTest () {
        cout << "destructor of the fixtures called" << endl;
    }

};

TEST_F (SequentialLinkedQueueTest, EmptyTestIntegers) {
    SequentialLinkedQueue <int> intQueue;

    // queue is initially empty
    EXPECT_EQ (true, intQueue.empty ());

    // after one enqueue/dequeue, the queue should be empty
    intQueue.enqueue (5);
    intQueue.dequeue ();
    EXPECT_EQ (true, intQueue.empty ());

    // after 500 enqueues and dequeue, the queue should be empty
    for (int i = 0; i < 500; i++) {
        intQueue.enqueue (i);
    }
    for (int i = 0; i < 500; i++) {
        intQueue.dequeue ();
    }
    EXPECT_EQ (true, intQueue.empty ());
}

TEST_F (SequentialLinkedQueueTest, DataTestIntegers) {
    SequentialLinkedQueue <int> intQueue;

    // after one enqueue, same value must come when dequeued
    intQueue.enqueue (5);
    EXPECT_EQ (5, intQueue.dequeue ());

    // now enqueue 500 items starting from 0.
    for (int i = 0; i < 500; i++) {
        intQueue.enqueue (i);
    }
    // the items should be dequeued accordingly
    for (int i = 0; i < 500; i++) {
        EXPECT_EQ (i, intQueue.dequeue ());
    }
}

TEST_F (SequentialLinkedQueueTest, RandomEnqueueDequeueIntegers) {
    SequentialLinkedQueue <int> intQueue;

    // now enqueue 1000 items starting from 0.
    for (int i = 0; i < 1000; i++) {
        intQueue.enqueue (i);
    }
    // dequeue 500 items
    for (int i = 0; i < 500; i++) {
        EXPECT_EQ (i, intQueue.dequeue ());
    }

    // enqueue 500 items
    for (int i = 0; i < 500; i++) {
        intQueue.enqueue (i);
    }

    // dequeue remaining items
    for (int i = 500; i < 1000; i++) {
        EXPECT_EQ (i, intQueue.dequeue ());
    }
    for (int i = 0; i < 500; i++) {
        EXPECT_EQ (i, intQueue.dequeue ());
    }


}


int main (int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}