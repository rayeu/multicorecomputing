#ifndef CUSTOMEXCEPTIONS_H
#define CUSTOMEXCEPTIONS_H

#include <iostream>
#include <exception>

using namespace std;


class NullException: public exception {
    virtual const char* what() const throw() {
        return "Queue Empty";
    }
} null;

#endif