
#include <iostream>

#include "sequential_queue.h"
#include "sequential_linked_queue.h"

using namespace std;

int main (void) {
    // SequentialQueue <int> queue;
    SequentialLinkedQueue <int> queue;

    queue.enqueue (5);
    queue.enqueue (5);
    queue.enqueue (5);
    queue.enqueue (4);
    queue.dequeue ();
    queue.dequeue ();
    // try {
    //     cout << queue.dequeue () << endl;
    //     cout << queue.dequeue () << endl;
    //     cout << queue.dequeue () << endl;
    //     cout << queue.dequeue () << endl;
    //     cout << queue.dequeue () << endl;
    //     cout << queue.dequeue () << endl;
    //     cout << queue.dequeue () << endl;
    // } catch (exception &e){
    //     cout << e.what () << endl;
    // }

    return 0;
}