/* Sequential Queue implementation using linked lists

Author: Ashok Adhikari
*/

#ifndef SEQUENTIALLINKEDQUEUE_H
#define SEQUENTIALLINKEDQUEUE_H

#include <exception>
#include "queue.h"
#include "custom_exceptions.h"

using namespace std;


template <typename T>
struct node {
    T data;
    struct node *next;
};


template <class T> 
class SequentialLinkedQueue {
private:
    struct node <T> *front;
    struct node <T> *rear;

public:
    SequentialLinkedQueue () {
        rear = front = nullptr;
    }

    ~SequentialLinkedQueue () {
        if (!empty ()) {
            while (front) {
                struct node <T> *current = front;
                front = front -> next;
                cout << "deleting node with data " << current -> data << endl;;
                delete current;    
            }
        }
    }

    void enqueue (T elem) {
        struct node <T> *newNode = new struct node <T>;
        newNode->data = elem;
        newNode->next = nullptr;

        if (!front) {
            rear = front = newNode;
        } else {
        rear->next = newNode;
        }

        rear = newNode;
    }

    T dequeue () {
        if (empty ()) throw null;

        T e = front->data;
        struct node <T> *toDel = front;
        front = front->next;
        delete toDel;
        return e;
    }

    bool empty () {
        return !front;
    }
};

#endif