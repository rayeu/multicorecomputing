#ifndef QUEUEINTERFACE_H
#define QUEUEINTERFACE_H

template <class T> class Queue {
public:
    virtual ~Queue () = 0;

    virtual void enqueue (T) = 0;
    virtual T dequeue () = 0;
    virtual bool empty () = 0;
};

template <class T>
inline Queue<T>::~Queue () {}

#endif