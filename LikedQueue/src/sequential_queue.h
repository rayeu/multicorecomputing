/* Sequential Queue implementation using stl list

Author: Ashok Adhikari
*/

#ifndef SEQUENTIALQUEUE_H
#define SEQUENTIALQUEUE_H

#include <list>
#include <exception>

#include "queue.h"
#include "custom_exceptions.h"

using namespace std;


template <class T> 
class SequentialQueue: public Queue <T>{
private:
    list<T> elements;
public:
    void enqueue (T elem) {
        elements.push_back (elem);
    }

    T dequeue () {
        if (empty ()) throw null;
        T e = elements.front ();
        elements.pop_front ();
        return e;
    }

    bool empty () {
        return elements.empty ();
    }
};

#endif