
#include <iostream>

#include "sequential_linked_queue.h"
#include "custom_exceptions.h"

using namespace std;

int main (void) {
    SequentialLinkedQueue <int> queue;

    queue.enqueue (5);
    queue.enqueue (5);
    queue.enqueue (5);
    queue.enqueue (4);
    queue.dequeue ();
    queue.dequeue ();
    try {
        cout << queue.dequeue () << endl;
        cout << queue.dequeue () << endl;
        cout << queue.dequeue () << endl;
        cout << queue.dequeue () << endl;
        cout << queue.dequeue () << endl;
        cout << queue.dequeue () << endl;
        cout << queue.dequeue () << endl;
    } catch (exception &e){
        cout << e.what () << endl;
    }

    return 0;
}