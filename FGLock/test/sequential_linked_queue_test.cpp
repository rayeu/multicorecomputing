#include <iostream>
#include <gtest/gtest.h>
#include <thread>
#include <vector>
#include <mutex>

#include "../src/sequential_linked_queue.h"


class SequentialLinkedQueueTest: public ::testing::Test {
public:
    SequentialLinkedQueueTest () {
    }

    void SetUp () {
    }

    void TearDown () {
    }

    ~SequentialLinkedQueueTest () {
    }

};

TEST_F (SequentialLinkedQueueTest, EmptyTestIntegers) {
    SequentialLinkedQueue <int> intQueue;

    int count = 10000;

    mutex m1;

    vector<int> numbers, results;
    for (int i = 0; i < count; i++) {
        numbers.push_back (i);
    }

    thread t1 ([&numbers, &intQueue] () {
        for (int i : numbers) {
            intQueue.enqueue (i);
        }
    });

    thread t2 ([&m1, count, &results, &intQueue] () {
        while (results.size () != count) {
            try {
                lock_guard <mutex> guard (m1);
                results.push_back(intQueue.dequeue ());
            } catch (exception &e) {
                // cout << e.what () << "thread 2" << endl;
            }
        }
    });

    thread t3 ([&m1, count, &results, &intQueue] () {
        while (results.size () != count) {
            try {
                lock_guard <mutex> guard (m1);
                results.push_back(intQueue.dequeue ());
            } catch (exception &e) {
                // cout << e.what () << "thread 3" << endl;
            }
        }
    });


    t1.join ();
    t2.join ();
    t3.join ();

    EXPECT_EQ (numbers.size (), results.size ());
    // test every element
    for (int i = 0; i < results.size (); i++) {
        EXPECT_EQ (numbers[i], results[i]);
    }
}

int main (int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}