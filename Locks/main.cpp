#include <iostream>
#include <chrono>
#include <thread>

#include "mapper.h"
#include "peterson.h"
#include "bakery.h"

using namespace std;

Mapper mapper;

const int numThreads = 5;

PetersonLock l1;
BakeryLock b1(numThreads);

void printFun () {
    b1.lock ();
    this_thread::sleep_for (chrono::seconds (0));
    cout << "I am having fun" << endl;
    cout << "Local ID for " << this_thread::get_id () << " is " << mapper.getLocalID (this_thread::get_id ()) << endl;
    b1.unlock ();
}

int main (int argc, char **argv) {
    
    // create threads
    thread t[numThreads];

    // now assign tasks to the threads
    for (int i = 0; i < numThreads; i++) {
        t[i] = thread (printFun);
    }

    for (int i = 0; i < numThreads; i++) {
        t[i].join ();
    }

    return 0;
}