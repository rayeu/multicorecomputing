#ifndef LOCK_H
#define LOCK_H

class Lock {
public:
    virtual ~Lock () = 0;
    virtual void lock () = 0;
    virtual void unlock () = 0;
};

inline Lock::~Lock () {}

#endif
