#ifndef MAPPER_H
#define MAPPER_H

#include <iostream>
#include <thread>
#include <map>
#include <mutex>

using namespace std;

class Mapper {
private:
    // maps threads global id to local ids of 0, 1, 2, ..
    map <thread::id, int> globalToLocalIDMap;

    int localID = 1;

    mutex m1;
public:
    void addThreadID (thread::id id) {
        lock_guard <mutex> guard (m1);
        globalToLocalIDMap[id] = localID++;
    }

    int getLocalID (thread::id id) {
        lock_guard <mutex> guard (m1);
        if (!globalToLocalIDMap[id]) {
            globalToLocalIDMap[id] = localID++;
        }
        return globalToLocalIDMap[id] - 1;
    }
};

#endif