#ifndef PETERSON_H
#define PETERSON_H

#include "lock.h"
#include "mapper.h"

class PetersonLock: public Lock {
private:
    bool flag[2] = {false, false};
    int victim;
    Mapper mapper;

public:
    void lock () {
        int i = mapper.getLocalID (this_thread::get_id ());
        int j = 1 - i;
        // I'm interested
        flag[i] = true;
        // but you go first
        victim = i;
        // now wait
        while (flag[j] && victim == i) {}
    }

    void unlock () {
        // release the lock
        int i = mapper.getLocalID (this_thread::get_id ());
        flag[i] = false;
    }

};

#endif