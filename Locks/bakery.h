#ifndef BAKERY_H
#define BAKERY_H

#include "lock.h"
#include "mapper.h"

class BakeryLock: public Lock {
private:
    bool *flag;
    int *label;

    int numThreads;

    Mapper mapper;

public:
    BakeryLock (int n) {
        flag = new bool[n];
        label = new int[n];

        for (int i = 0; i < n; i++) {
            flag[i] = false;
            label[i] = 0;
        }

        numThreads = n;
    }

    ~BakeryLock () {
        delete flag;
        delete label;
    }

    void lock () {
        int i = mapper.getLocalID (this_thread::get_id ());
        // I want to go
        flag[i] = true;
        // new label
        label[i] = getMaxLevel () + 1;

        bool wait = true;
        while (wait) {
            wait = false;
            for (int k = 0; k < numThreads; k++) {
                if (k == i) continue;
                if (flag[k] && (label[k] < label[i] || (label[k] == label[i] && k < i))) {
                    // wait until there is another thread of smaller label who wants to go
                    wait = true;
                    continue;
                }
            }
        }
    }

    void unlock () {
        int i = mapper.getLocalID (this_thread::get_id ());
        flag[i] = false;
    }

    int getMaxLevel () {
        int max = 0;
        for (int i = 0; i < numThreads; i++) {
            if (label[i] > max) {
                max = label[i];
            }
        }
        return max;
    }
};

#endif